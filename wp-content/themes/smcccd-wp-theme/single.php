<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package smccd
 */

get_header(); ?>
<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<h1 class="single-page-category"><?php single_cat_title() ?> Posts</h1>
			<?php while ( have_posts() ) : the_post(); ?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<p class=>Written by <span class="author-name"><strong><?php the_author(); ?></strong></span> on <date><strong><?php the_date(); ?></strong></date></p>
				<div class="container single-container">
					<p><?php the_content(); ?></p>
				</div>
		<?php
			endwhile; // End of the loop.
		?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
//get_sidebar();
get_footer();