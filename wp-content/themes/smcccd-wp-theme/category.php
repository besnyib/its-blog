<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package smccd
 */

get_header(); ?>
<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<h1 class="category-title"><?php single_cat_title() ?> Posts</h1>
			<p>This web page contains all posts from the Information Technology Services (ITS) blog that have been tagged as "<em><?php single_cat_title() ?></em>" from all users.</p>
			<hr>
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="category-post">
				<div>
					<h3 class="category-page-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<p class="category-author-date">by <span class="author-name"><strong><?php the_author(); ?></strong></span> written on <date><strong><?php the_date(); ?></strong></date></p>
					<hr class="category-rule">
				<div class="category-content-block">
					<p><?php the_excerpt(); ?></p>
				</div>
				</div>
				<div class="read_more">
					<a class="btn btn-primary" href="<?php the_permalink(); ?>">Full Post on "<span class="title_bold"><?php the_title(); ?></span>"</a>
				</div>
			</div>
		<?php
			endwhile; // End of the loop.
		?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php
//get_sidebar();
get_footer();