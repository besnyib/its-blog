jQuery(document).ready(function($) {

  var rel = $('body').attr('rel');

  $('li#'+rel).addClass("active");

});

jQuery(document).ready(function($) {
  function markActiveLink() {

    path = top.location.pathname;

    $("ul.section-menu-nav").find("a").filter(function() {
      var href = $(this).attr('href');

            //check if file has extension and doesn't end
            //in a forward slash, add one plus the default file name
            if (/\.[a-zA-Z0-9]{3,5}$/.test(href) == false) {

              if (/\/$/.test(href) == false) {
                href = href + '/index.php';
              }
            }

            //if the path is a folder, select default file type
            if (/\.[a-zA-Z0-9]{3,5}$/.test(path) == false) {

              path = path + 'index.php';
            }


            return href != '/' && href != '#' && path.indexOf(href) == 0;
          }).parent('li').addClass("active");



        //Afterwards, look back through the links. If none of them were marked,
        //mark your default one.

        if ($("ul.section-menu-nav li").hasClass("active") == false) {
          $("ul.section-menu-nav li:first").addClass("active");
        }


      }



      markActiveLink();

      $("ul.section-menu-nav .active > ul").show();
      $("ul.section-menu-nav ul li.active").parent().parent().addClass('active');
      $("ul.section-menu-nav ul li.active").parent().parent().parent().parent().addClass('active');
      $("ul.section-menu-nav li ul").parent().children('a').children('i').attr('class', 'icon-expand-alt').append('</span>');
      $("ul.section-menu-nav ul .active").parent('ul').show();
      $("ul.section-menu-nav li.active ul").parent().children('a').children('i').attr('class', 'icon-double-angle-down');


    });

(function(h) {
  h.deparam = function(i, j) {
    var d = {}, k = {
      "true": !0,
      "false": !1,
      "null": null
    };
    h.each(i.replace(/\+/g, " ").split("&"), function(i, l) {
      var m;
      var a = l.split("="),
      c = decodeURIComponent(a[0]),
      g = d,
      f = 0,
      b = c.split("]["),
      e = b.length - 1;
      /\[/.test(b[0]) && /\]$/.test(b[e]) ? (b[e] = b[e].replace(/\]$/, ""), b = b.shift().split("[").concat(b), e = b.length - 1) : e = 0;
      if (2 === a.length) if (a = decodeURIComponent(a[1]), j && (a = a && !isNaN(a) ? +a : "undefined" === a ? void 0 : void 0 !== k[a] ? k[a] : a), e) for (; f <= e; f++) c = "" === b[f] ? g.length : b[f], m = g[c] = f < e ? g[c] || (b[f + 1] && isNaN(b[f + 1]) ? {} : []) : a, g = m;
      else h.isArray(d[c]) ? d[c].push(a) : d[c] = void 0 !== d[c] ? [d[c], a] : a;
      else c && (d[c] = j ? void 0 : "")
    });
    return d
  }
})(jQuery); 

function uniq(a) {
    var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

    return a.filter(function(item) {
        var type = typeof item;
        if(type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
        else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
}