/********************************************************
 *
 * Custom Javascript code for AppStrap Bootstrap theme
 * Written by Themelize.me (http://themelize.me)
 *
 *******************************************************/
/*global jRespond */
jQuery(document).ready(function($) {
  "use strict";

    function getColorFromSlide(slide){

        var sourceImage = extractUrl(slide.children('.background-image').css('background-image'));

        if(location.host.indexOf('www.') >= 0 && sourceImage.indexOf('www.') < 0){
            sourceImage = sourceImage.replace("//", "//www.");
        }
        if(location.host.indexOf('www.') < 0 && sourceImage.indexOf('www.') >= 0){
            sourceImage = sourceImage.replace("/www.", "/");
        }

        var isHost = (sourceImage.indexOf(location.host) >= 0);
        var isRelative = (sourceImage.indexOf('//') < 0);

        if(isHost || isRelative){
            var colorThief = new ColorThief();
            var oImg=document.createElement("img");
            oImg.setAttribute('src', sourceImage);
            var result = colorThief.getColor(oImg);

            $(slide).attr('data-bg-color', "rgb("+result[0]+","+result[1]+","+result[2]+")");
        }



    }

    function extractUrl(input)
    {
        // remove quotes and wrapping url()
        return input.replace(/"/g,"").replace(/url\(|\)$/ig, "");
    }


    //IE placeholders
  // --------------------------------
  $('html.lt-ie8 [placeholder]').focus(function() {
    var input = $(this);
    if (input.val() === input.attr('placeholder')) {
      if (this.originalType) {
        this.type = this.originalType;
        delete this.originalType;
      }
      input.val('');
      input.removeClass('placeholder');
    }
  }).blur(function() {
    var input = $(this);
    if (input.val() === '') {
      input.addClass('placeholder');
      input.val(input.attr('placeholder'));
    }
  }).blur();
  
  // Detect Bootstrap fixed header
  // @see: http://getbootstrap.com/components/#navbar-fixed-top
  // --------------------------------
  if ($('.navbar-fixed-top').size() > 0) {
    $('html').addClass('has-navbar-fixed-top');
  }
  
  // Bootstrap tooltip
  // @see: http://getbootstrap.com/javascript/#tooltips
  // --------------------------------
  // invoke by adding data-toggle="tooltip" to a tags (this makes it validate)
  if(jQuery().tooltip) {
    $('body').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
    });
  }
    
  // Bootstrap popover
  // @see: http://getbootstrap.com/javascript/#popovers
  // --------------------------------
  // invoke by adding data-toggle="popover" to a tags (this makes it validate)
  if(jQuery().popover) {
    $('body').popover({
      selector: "[data-toggle=popover]",
      container: "body",
      trigger: "hover"
    });
  }

  //allow any page element to set page class
  // --------------------------------  
  $('[data-page-class]').each(function() {
    $('html').addClass($(this).data('page-class'));
  });
  
  //show hide for hidden header
  // --------------------------------
  $('[data-toggle=show-hide]').each(function() {
    $(this).click(function() {
      var state = 'open'; //assume target is closed & needs opening
      var target = $(this).attr('data-target');
      var targetState = $(this).attr('data-target-state');
      
      //allows trigger link to say target is open & should be closed
      if (typeof targetState !== 'undefined' && targetState !== false) {
        state = targetState;
      }
      
      if (state === 'undefined') {
        state = 'open';
      }
      
      $(target).toggleClass('show-hide-'+ state);
      $(this).toggleClass(state);
    });
  });



  //Plugin: jPanel Menu
  // data-toggle=jpanel-menu must be present on .navbar-btn
  // @todo - allow options to be passed via data- attr
  // --------------------------------
if(!$('html').hasClass('lt-ie8')){

  var menuTrigger = $('[data-toggle=jpanel-menu]');

  var slideMenu = menuTrigger.data('target');

    //jRespond settings
    var jRes = jRespond([
      {
        label: 'large',
        enter: 0,
        exit: 767
      }
    ]);
    
    //turn jPanel Menu on/off as needed
    jRes.addFunc({
        breakpoint: 'large',
        enter: function() {
          
          $(slideMenu).clone().attr('id', 'MMenu' ).addClass("mm-ismenu").insertBefore(slideMenu);
          //$(slideMenu).clone().attr('data-menu', 'MMenu').addClass("mm-ismenu").insertBefore(slideMenu);

          $('#MMenu ul.nav-pills').addClass('nav-stacked');

          var $MMenu = $('#MMenu').mmenu({
            clone : true,
            dragOpen: true
          }, {
             // configuration:
             selectedClass  : "Selected",
             labelClass     : "Label",
             panelClass     : "Panel"
          });

          $(this).trigger( "open.mm" );
        },
        exit: function() {
          $('#MMenu').trigger("close.mm").remove();
        }
    });

}

  //Plugin: clingify (sticky navbar)
  // --------------------------------
  if (jQuery().clingify) {
    $('html:not(.lt-ie9) [data-toggle=clingify]').clingify({
      breakpoint: 751,
      resized : function() {
        
      },
      locked : function() {
        $('.js-clingify-placeholder').height($('.top-bar').innerHeight());
      },
      detached : function() {
        
      }
    });

  }

//Plugin: flexslider
    // --------------------------------
    $('.flexslider').each(function() {
        var sliderSettings =  {
            animation: $(this).attr('data-transition'),
            selector: ".slides > .slide",
            controlNav: true,
            smoothHeight: true,
            start: function(slider) {



                var firstSlide = slider.find('.slide').eq(0);



                //hide all animated elements
                slider.find('[data-animate-in]').each(function() {
                    $(this).css('visibility','hidden');
                });


                if($(firstSlide).data('bg-color') !== undefined){
                    slider.closest('.apply-color-thief').animate({
                        backgroundColor: $(firstSlide).attr('data-bg-color')
                    }, 0);
                }

                //animate in first slide
                slider.find('.slide').eq(1).find('[data-animate-in]').each(function() {
                    $(this).css('visibility','hidden');
                    if ($(this).data('animate-delay')) {
                        $(this).addClass($(this).data('animate-delay'));
                    }
                    if ($(this).data('animate-duration')) {
                        $(this).addClass($(this).data('animate-duration'));
                    }
                    $(this).css('visibility','visible').addClass('animated').addClass($(this).data('animate-in'));
                    $(this).one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
                        function() {
                            $(this).removeClass($(this).data('animate-in'));
                        }
                    );
                });
            },
            before: function(slider) {



                if(slider.find('.slide').eq(slider.animatingTo).data('bg-color') !== undefined){
                    slider.closest('.apply-color-thief').animate({
                        backgroundColor: slider.find('.slide').eq(slider.animatingTo).data('bg-color')
                    }, 1000);
                }




                //hide next animate element so it can animate in
                slider.find('.slide').eq(slider.animatingTo + 1).find('[data-animate-in]').each(function() {
                    $(this).css('visibility','hidden');
                });
            },
            after: function(slider) {
                //hide animtaed elements so they can animate in again
                slider.find('.slide').find('[data-animate-in]').each(function() {
                    $(this).css('visibility','hidden');
                });

                //animate in next slide
                slider.find('.slide').eq(slider.animatingTo + 1).find('[data-animate-in]').each(function() {
                    if ($(this).data('animate-delay')) {
                        $(this).addClass($(this).data('animate-delay'));
                    }
                    if ($(this).data('animate-duration')) {
                        $(this).addClass($(this).data('animate-duration'));
                    }
                    $(this).css('visibility','visible').addClass('animated').addClass($(this).data('animate-in'));
                    $(this).one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
                        function() {
                            $(this).removeClass($(this).data('animate-in'));
                        }
                    );
                });
            }
        };

        var sliderNav = $(this).attr('data-slidernav');
        if (sliderNav !== 'auto') {
            sliderSettings = $.extend({}, sliderSettings, {
                manualControls: sliderNav +' li a',
                controlsContainer: '.flexslider-wrapper'
            });
        }

        $('html').addClass('has-flexslider');
        $(this).flexslider(sliderSettings);
    });
    $('.flexslider').resize(); //make sure height is right load assets loaded
  


});


