//Plugin: jQuery Isotope plugin
// --------------------------------
jQuery(document).ready(function($) {

    // switches selected class on buttons
    function changeSelectedLink( $elem ) {
        // remove selected class on previous item
        $elem.parents('li').parent('ul').find('li').removeClass('active');
        // set selected class on new item
        $elem.parent('li').addClass('active');
    }


    $('html:not(.lt-ie8) [data-js=isotope]').each(function() {


        var $container = $(this);

        $(this).hide();
        var $isotopeSelector = $(this).data('item-selector');

        var $isotopeTrigger =  $(this).data('isotope-trigger');

        var $layoutType = 'fitRows';

        if($(this).data('layout-type')){
            $layoutType = $(this).data('layout-type');
        }

        $(this).show();


    var isIsotopeInit = false;

    function onHashchange() {

        var hashFilter = getHashFilter();
        if ( !hashFilter && isIsotopeInit ) {
            hashFilter = '*';
        }
        isIsotopeInit = true;
        // filter isotope
        var $grid = $container.isotope({
            itemSelector: $isotopeSelector,
            filter: hashFilter,
            layoutMode: $layoutType
        });

        // layout Isotope after each image loads
        $grid.imagesLoaded().progress( function() {
            $grid.isotope('layout');
        });

        // set selected class on button
        if ( hashFilter ) {
            changeSelectedLink($('a[data-filter="'+hashFilter+'"]'));
        }


    }

        $(window).on( 'hashchange', onHashchange );
        // trigger event handler to init Isotope
        onHashchange();
    });
});

function getHashFilter() {
    var hash = location.hash;
    // get filter=filterName
    var matches = location.hash.match( /filter=([^&]+)/i );
    var hashFilter = matches && matches[1];
    return hashFilter && decodeURIComponent( hashFilter );
}