<?php 

/** Template Name: Web Service Status Template 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smccd
 */

get_header(); ?>
    <div class="container">
        <div id="primary" class="content-area row">
            <main id="main" class="site-main col-md-12" role="main">
                <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'wsstatus' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <?php
get_footer();