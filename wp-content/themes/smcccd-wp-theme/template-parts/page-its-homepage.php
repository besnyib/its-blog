<?php 

/** Template Name: ITS Homepage Template 2
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smccd
 */

get_header(); ?>

    <div class="container">
        <div id="primary" class="content-area">
            <div class="row">
                <main id="main" class="site-main col-md-12" role="main">
                    <?php
    			while ( have_posts() ) : the_post();
    				get_template_part( 'template-parts/content', 'its-homepage' );

    				// If comments are open or we have at least one comment, load up the comment template.
    				if ( comments_open() || get_comments_number() ) :
    					comments_template();
    				endif;

    			endwhile; // End of the loop.
    			?>
                </main>
            </div>
            <div class="container col-md-8">
                <?php
                    get_template_part( 'template-parts/content', 'its-feature-element');
                ?>
           
            <div class="row col-container">
                <div class="col-md-6">
                    <div class="feed-items-widget">
                        <div class="feed-type-announcements col">
                            <?php
                                get_template_part( 'template-parts/content', 'its-homepage-announcements' );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="feed-items-widget">
                        <div class="feed-type-faqs col">
                            <?php
                                get_template_part( 'template-parts/content', 'its-homepage-faqs' );
                            ?>
                        </div>
                    </div>
                </div>
                <!-- #main -->                
            </div>
        </div>
        <aside class="col-md-4"><?php get_sidebar('home'); ?></aside>
        <!-- #primary -->
    </div>
    <?php
get_footer();