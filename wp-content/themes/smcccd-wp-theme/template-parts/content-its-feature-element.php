<div class="feature-box">

    <?php 
    	query_posts('posts_per_page=1&cat=12');
    	if ( have_posts() ) while ( have_posts() ) : the_post();
    ?>

    <h3 class="feature-title">Featured Post</h3>
    <span class="feed-item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
    <p><small>by <?php the_author(); ?> on <date><?php the_date(); ?></date></small></p>
    <span class="feed-item-excerpt">
    	<?php the_excerpt(); ?>
    </span></p>
    <p class="buttons"><a class="frontpage-button btn btn-primary" href="<?php the_permalink(); ?>">Read more about <strong>"<?php the_title(); ?>"</strong></a>

    <?php endwhile; ?>
    <?php wp_reset_query(); ?>

</div>