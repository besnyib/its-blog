  <div id="Header" >
    <div class="top-bar" id="TopBar" data-toggle="clingify">
     <div class="slidedown">
       <a class="navbar-btn" data-toggle="jpanel-menu" data-target="#MainNavigation" href="#MMenu">
       <span class="sr-only">Open Nav</span> <span class="bar"></span> <span class="bar"></span> <span class="bar"></span> <span class="bar"></span> </a>
       <div class="container block">

        <div class="pull-left">

          <div class="btn-group pull-left">
            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
              Change&nbsp;Site&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
            </button>


            <ul class="dropdown-menu" role="menu">

              <li><a href="https://websmart.smccd.edu" role="listitem" ><i class="fa fa-lg fa-gear"></i>&nbsp;&nbsp;WebSMART</a></li>
              <li><a href="https://smccd.mrooms.net" role="listitem"><i class="fa fa-lg fa-globe"></i>&nbsp;&nbsp;WebAccess</a></li>

              <li><a href="http://my.smccd.edu" role="listitem"><i class="fa fa-lg fa-envelope"></i>&nbsp;&nbsp;My.SMCCD</a></li>
              <li><a href="http://smccd.edu/portal" role="listitem"><i class="fa fa-lg fa-columns"></i>&nbsp;&nbsp;Portal</a></li>
              <li><a href="http://webschedule.smccd.edu" role="listitem"><i class="fa fa-lg fa-table"></i>&nbsp;&nbsp;WebSchedule</a></li>

              <li class="divider"></li>
              <li class="li-csm" role="listitem"><a href="http://collegeofsanmateo.edu"><i class="fa fa-lg fa-university"></i>&nbsp;&nbsp;College of San Mateo</a></li>
              <li class="li-can" role="listitem"><a href="http://canadacollege.edu"><i class="fa fa-lg fa-university"></i>&nbsp;&nbsp;Cañada College</a></li>
              <li class="li-sky" role="listitem"><a href="http://skylinecollege.edu"><i class="fa fa-lg fa-university"></i>&nbsp;&nbsp;Skyline College</a></li>


            </ul>

          </div>
          <div class="pull-left hidden-xs" style="margin-top:3px; padding-left: 3px"> | SMCCCD</div>
        </div>

        <div class="pull-right">
          <ul class="search-tools navbar-left">
           <li><a href="http://directory.smccd.edu"><span class="sr-only">District Directory</span><i class="fa fa-lg fa-user"></i></a></li>
           <li><a href="//smccd.edu/search/azindex.php"><span class="sr-only">Site Index</span><i class="fa fa-lg fa-sort-alpha-asc"></i></a></li>
         </ul>
         <div class="search-form navbar-right hidden-xs">
          <form action="/search/index.php" role="form" style="margin:0;padding:0">

            <input type="hidden" name="cx" value="001343176506729611594:dxf40qjualg" />
            <input type="hidden" name="cof" value="FORID:11" />
            <input type="hidden" name="ie" value="UTF-8" />

            <div class="input-group">
              <label class="sr-only" for="as_q">Search site using Google...</label>
              <input type="text" class="form-control input-sm" id="as_q" name="q" placeholder="Search site using Google...">
              <span class="input-group-btn">
                <button class="btn btn-primary btn-sm" type="submit"><span class="sr-only">Search</span><i class="fa fa-search"></i></button>
              </span>
            </div>

          </form>


        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
<!--Header & Branding region-->
<div class="header header-inverse">

  <div class="header-inner container">
    <div class="row navbar-bar navbar-horizontal-lg">
      <div class="smccd-logo">
        <!--branding/logo-->
        <a class="text-hide container-logo-smccd" href="http://smccd.edu/" title="Home">
          <img class="logo-smccd" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="SMCCCD Logo" />
        </a>
      </div>



      <!--header rightside-->

      <div class="block-nav" >

        <div class="navbar-collapse collapse" id="MainNavigation">

          <form action="//smccd.edu/search/index.php" role="form" class="visible-xs navbar-left">

            <input type="hidden" name="cx" value="001343176506729611594:dxf40qjualg" />
            <input type="hidden" name="cof" value="FORID:11" />
            <input type="hidden" name="ie" value="UTF-8" />

            <div class="input-group">
              <label class="sr-only" for="as_q2">Search site using Google...</label>
              <input type="text" class="form-control input-sm" id="as_q2" name="q" placeholder="Search site through Google...">
              <span class="input-group-btn">
                <button class="btn btn-primary btn-sm" type="submit"><span class="sr-only">Search</span><i class="fa fa-search"></i></button>
              </span>
            </div>

          </form>

          <ul class="nav nav-pills nav-mf">
            <li class="about-smcccd" id="SectionAbout"><a href="//smccd.edu/accounts/smccd/aboutus/">About SMCCCD</a></li>
            <li class="dropdown" id="SectionBoardOfTrustees"><a href="//smccd.edu/boardoftrustees" id="bot-drop" >Board of Trustees</a></li>
            <li class="dropdown" id="SectionDepartments"><a id="dept-drop"  href="//smccd.edu/departments/">Departments</a></li>
            <li><a href="//jobs.smccd.edu/" id="SectionEmployment">Employment</a> </li>
            <li><a href="//www.smcccfoundation.org/" id="SectionFoundation">Foundation</a> </li>
            <li><a href="//smccd.edu/accounts/smccd/aboutus/contactus.shtml" id="SectionContact">Contact SMCCCD</a></li>


          </ul>
        </div>
      </div>

    </div>

  </div>
</div>


</div>