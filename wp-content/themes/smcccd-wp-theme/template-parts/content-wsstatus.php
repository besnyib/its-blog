<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smccd
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php // Getting Notifications Posts
		$args = array(
			'post_type' => 'notifications',
			'category__not_in' => get_cat_ID('archived'),
			);
		$query = new WP_Query ($args);
		while ( $query->have_posts() ):
			$query->the_post();
			$notiTerms = wp_get_post_terms(get_the_ID(), 'smccd_notice_styles');
			$styleTerms = "";
			foreach ($notiTerms as $key => $value) {
				$styleTerms .= $value->slug . " ";
			}

			echo '<div class="alert ' . $styleTerms . '">'; 
			echo get_the_content();
			
				$impactTerms = wp_get_post_terms(get_the_ID(), 'smccd_impacted_services');
				if($impactTerms){
					echo '<div class="impacted">';
					echo '<span class="title">Impacted Services: </span>';
					echo '<ul class="fa-ul impacted-services">';
					foreach ($impactTerms as $key => $value) {
						echo '<li class="fa-li fa fa-arrow-circle-o-down text-danger"> ' . $value->name . '</li>';
					}
					echo '</ul>';
					echo '</div>';
				}

			echo '</div>';


		endwhile;
		wp_reset_postdata();
	?>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'smccd' ),
				'after'  => '</div>',
			) );
		?>

	</div><!-- .entry-content -->

	


	<section id="incidents" class="incidents">
		<h1>Past Incidents</h1>
		<?php
			$args = array(
			'post_type' => 'notifications',
			'category__in' => get_cat_ID('archived'),
			'post_per_page' => 5,
			);
			$query = new WP_Query ($args);
			while ( $query->have_posts() ):
				$query->the_post();
				$impactTerms = [];
				$notiTerms = [];
				$notiTerms = wp_get_post_terms(get_the_ID(), 'smccd_notice_styles');
				$styleTerms = "";
				foreach ($notiTerms as $key => $value) {
					$styleTerms .= $value->slug . " ";
				}
				$impactTerms = wp_get_post_terms(get_the_ID(), 'smccd_impacted_services');
				?>
				<div class="incident">
					<div class="date"><?php the_date(); ?></div>
					<!-- <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> -->
					<div class="content"><?php the_content(); ?></div>
					<?php
						if($impactTerms){
								echo '<div class="impacted">';
								echo '<span class="title">Impacted Services: </span>';
								echo '<ul class="fa-ul impacted-services">';
								foreach ($impactTerms as $key => $value) {
									echo '<li class="fa-li fa fa-arrow-circle-o-down text-danger"> ' . $value->name . '</li>';
								}
								echo '</ul>';
								echo '</div>';
							}
						?>
					<div class="time"><?php the_time('M j, Y - g:i a T'); ?></div>
					<div class="category">Category in: <?php the_category(', ');?></div>
				</div>

		<?php endwhile;
			wp_reset_postdata(); 
		?>
	</section>

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'smccd' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
