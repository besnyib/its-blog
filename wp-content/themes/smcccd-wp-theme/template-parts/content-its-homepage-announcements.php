<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smccd
 */

?>

<div class="feed-items-widget">
	<div class="block-default info-block with-icon">
		<h3>Announcements</h3>
		<div class="caption">
	    <ul>

			<?php query_posts('posts_per_page=5&cat=2');
			if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	     	<li><span class="feed-item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span> <date><?php the_date(); ?></date> </li>
	      	
	      	<?php endwhile; ?>
	      	<?php wp_reset_query(); ?>
	      	
	    </ul>
	  </div>
	  
	<p class="buttons"><a href="https://its.smccd.edu/category/announcements" class="frontpage-button btn btn-primary"><span class="icon-angle-right">Read more Announcements</span></a></p>
</div>


</div>
