<?php 

/** Template Name: ITS Homepage Template sub
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smccd
 */

get_header(); ?>

    <div class="container">
        <div id="primary" class="content-area">
            <main id="main" class="site-main col-md-8" role="main">
                
                <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'its-subpage' );

				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
            </main>
            <!-- #main -->
            <aside class="col-md-4"><?php get_sidebar('home'); ?></aside>
        </div>
        <!-- #primary -->
    </div>
    
    <?php
get_footer();