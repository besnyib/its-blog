<?php
/**
 * smccd functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package smccd
 */

if ( ! function_exists( 'smccd_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function smccd_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on smccd, use a find and replace
	 * to change 'smccd' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'smccd', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'smccd' ),
		'secondary' => esc_html__( 'Secondary', 'smccd' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'smccd_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

}
endif;
add_action( 'after_setup_theme', 'smccd_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function smccd_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'smccd_content_width', 640 );
}
add_action( 'after_setup_theme', 'smccd_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function smccd_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'smccd' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'smccd' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar-Home', 'smccd' ),
		'id'            => 'sidebar-home',
		'description'   => esc_html__( 'Add widgets here.', 'smccd' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'smccd_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function smccd_scripts() {
	wp_enqueue_style( 'smccd-style', get_stylesheet_uri() );
	wp_enqueue_style( 'smccd-font', '//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Lora' );
	wp_enqueue_style( 'smccd-font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css' );
	wp_enqueue_style( 'smccd-bootstrap', get_template_directory_uri() . '/dist/smcccd-bootstrap.css' );
	wp_enqueue_style( 'smccd-theme', get_template_directory_uri() . '/dist/smcccd-theme.css' );

	wp_enqueue_script( 'modernizer', '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js', array('jquery'), '20160725', true );
	wp_enqueue_script( 'smccd-bundle', get_template_directory_uri() . '/dist/bundle.js', array('jquery'), '20160725', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'smccd_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Utilities Functions.
 */
require get_template_directory() . '/inc/utils.php';

/**
 * Load Custom Navigation Walker.
 */
require get_template_directory() . '/inc/nav.php';

/**
 * Load Custom Navigation Walker.
 */
require get_template_directory() . '/inc/breadcrumbs.php';

function smccd_add_taxonomies_to_pages() {
 register_taxonomy_for_object_type( 'post_tag', 'page' );
 register_taxonomy_for_object_type( 'category', 'page' );
 }
add_action( 'init', 'smccd_add_taxonomies_to_pages' );

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
       global $post;
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


// Replaces the excerpt "Read More" text by a link - Original
// function new_excerpt_more($more) {
//        global $post;
// 	return '<a class="moretag" href="'. get_permalink($post->ID) . '">... Continued</a>';
// }
// add_filter('excerpt_more', 'new_excerpt_more');

// Function to change email address
 
function wpb_sender_email( $original_email_address ) {
    return 'itsblog@smccd.edu';
}
 
// Function to change sender name
function wpb_sender_name( $original_email_from ) {
    return 'SMCCD ITS';
}
 
// Hooking up our functions to WordPress filters 
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );