Based on SMCCCD LESS with _s (Underscore Sass Theme)
=======

San Mateo County Community College District Website Theme Built in LESS
-----------

###Configuration

  You will need:

  * Git
  * Grunt
  * NodeJS / Npm

  You can use the demo folder to test new styles.
  
  Run the npm installers to download all dependencies:
  
  `npm install`

###Development mode

  `npm run-script test`

###Production

   `npm run-script prod`
  
    The `dist` directory will contain 2 CSS files and 1 bundle.js file.  

