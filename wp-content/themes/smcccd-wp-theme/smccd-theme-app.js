/**
 * Created by smithchr on 8/26/2015.
 */
'use strict';
require("./less/smccd-template.less");
require("./Plugin/jQuery.mmenu/src/css/jquery.mmenu.all.css");
require("./Plugin/clingify/clingify.css");

require("script!./Plugin/jquery-migrate/jquery-migrate.min.js");
require("script!./Plugin/bootstrap/dist/js/bootstrap.min.js");
require("script!./Plugin/jRespond/js/jRespond.min.js");
require("script!./Plugin/jQuery.mmenu/src/js/jquery.mmenu.min.js");
require("script!./Plugin/clingify/jquery.clingify.js");
require("script!./Plugin/history.js/scripts/compressed/history.js");

require("script!./Plugin/flexslider/jquery.flexslider.js");
 
require("script!./Plugin/imagesloaded/imagesloaded.pkgd.js");
require("script!./Plugin/hammerjs/hammer.min.js");
require("script!./Plugin/isotope/dist/isotope.pkgd.min.js");

require("script!./js/smccd.js");
require("script!./js/script.js");
require("script!./js/isotope.js"); 