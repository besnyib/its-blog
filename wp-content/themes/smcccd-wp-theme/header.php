<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package smccd
 */

?>
    <!--[if lt IE 7]> <html lang="en-us" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]> <html lang="en-us" class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]> <html lang="en-us" class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!-->
    <html lang="en-us" class="no-js" <?php language_attributes(); ?>>
    <!--<![endif]-->

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="page" class="site">
            <a class="skip-link screen-reader-text sr-only" href="#content">
                <?php esc_html_e( 'Skip to content', 'smccd' ); ?>
            </a>
            <?php get_template_part('template-parts/smccd-header'); ?>
            <div id="breadcrumb" class="breadcrumb-wrapper">
                <div class="container">
                    <div class="clearfix">
                        <div class="call-to-action pull-left">
                            <h1 class="section-title"><?php bloginfo( 'name' ); ?></h1>
                            <?php
                $description = get_bloginfo( 'description', 'display' );
                if ( $description || is_customize_preview() ) : ?>
                                <p class="site-description">
                                    <?php echo $description; /* WPCS: xss ok. */ ?>
                                </p>
                                <?php
                endif; ?>
                        </div>
                        <div class="pull-right hidden-xs">
                            <form role="search" method="get" class="search-form form-inline" style="margin-top:35px;" action="http://smccd.edu/its-resources/">
                                <div class="input-group">
                                    <label for="s" class="hide">Search for:</label>
                                    <input id="s" type="search" value="" name="s" class="form-control" placeholder="Search ITS"  aria-label="Search through site content">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit" style="padding:9px;"><span class="fa fa-search"></span><span class="sr-only">search</span></button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- .site-branding -->
                    <?php //smccd_breadcrumbs(); ?>
                    <?php
                            if ( function_exists('yoast_breadcrumb') ) {
                                    yoast_breadcrumb('
                                    <p id="breadcrumbs">','</p>
                                            ');
                                    }
                            ?>
                </div>
            </div>
            <!-- #masthead -->
            <nav class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#LocalNavigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="LocalNavigation" class="collapse navbar-collapse" aria-expanded="true">
                        <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav navbar-nav section-menu-nav'));?>
                        <?php wp_nav_menu(array('theme_location' => 'secondary', 'menu_class' => 'nav navbar-nav section-menu-nav navbar-right'));?>
                    </div>
                </div>
            </nav>
            <!-- #site-navigation -->
            <div id="" class="site-content">