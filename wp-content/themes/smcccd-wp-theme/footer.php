<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package smccd
 */

?>

	</div><!-- #content -->

	   <footer class="theme theme-inverse">
         <div class="footer-theme-top">
            <div class="container">
               <div class="row">
                  <div class="col-sm-6">
                     <div class="footer-column">
                        <h3>San Mateo County CCD</h3>
                        <p><a href="//smccd.edu" >SMCCCD</a> are a three College District located between San Francisco and the Silicon Valley. Our Colleges serve more than 40,000 students each year and offer the first two years of instruction in a wide variety of transfer programs as well as more than 90 vocational-technical programs.</p>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="footer-column">

                        <h3>Contact</h3>
                           <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;3401 CSM Drive, <br>
                              &nbsp;&nbsp;&nbsp;&nbsp;San Mateo, CA 94402 USA <br>
                              <i class="fa fa-phone"></i><span>&nbsp;&nbsp;(650) 574-6550 </span><br>
                              <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;<a href="mailto:webmaster@smccd.edu">webmaster@smccd.edu</a>
                           </p>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        
                        <h3>smccd.edu</h3>
                        <div class="footer-column">
                           <ul class="list-unstyled">
                              <li><a href="//smccd.edu/accounts/smccd/"><span class="fa fa-angle-right"></span>&nbsp;&nbsp;Home</a></li>
                              <li><a href="//smccd.edu/boardoftrustees/"><span class="fa fa-angle-right"></span>&nbsp;&nbsp;Board of Trustees</a></li>
                              <li><a href="//smccd.edu/departments/"><span class="fa fa-angle-right"></span>&nbsp;&nbsp;Departments</a></li>
                              <li><a href="http://jobs.smccd.edu" ><span class="fa fa-angle-right"></span>&nbsp;&nbsp;Employment</a></li>
                              <li><a href="http://www.smcccfoundation.org/"><span class="fa fa-angle-right"></span>&nbsp;&nbsp;Foundation</a></li>
                              <li>
                                <a target="_blank" href="http://smccd.edu/titleix"><span class="fa fa-angle-right"></span>&nbsp;&nbsp;Title IX</a>
                              </li>
                              <li><a href="http://smccd.edu/accessibility/"><span class="fa fa-angle-right"></span>&nbsp;&nbsp;Accessibility</a></li>
                              <li><a href="https://smccd.edu/accounts/smccd/aboutus/contactus.shtml"><span class="fa fa-angle-right"></span>&nbsp;&nbsp;Contact</a></li>
                           </ul>     
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="footer-theme-bottom copyright-details">
               <div class="container">
                 
                        <span>Copyright <a href="https://its.smccd.edu/wp-admin">&copy;</a> <a href="http://www.smccd.edu" target="blank">SMCCCD</a> &mdash; All rights reserved.</span>
                        
               </div>
            </div>
         </footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
