<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'its');

/** MySQL database username */
define('DB_USER', 'itsuser');

/** MySQL database password */
define('DB_PASSWORD', 'itspassword');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=vkV,v0L<sR<b5:%ZaC0z|syK(%U]u>-`&0o`2HgW9`bSF2,ft{A! &A)$K$~5m.');
define('SECURE_AUTH_KEY',  'qFJY?~hfE98,J]S#=T9+=KWj<]Pdun)w{2C=k)bc_;H:nu.2b/P6d~DMa/6hye-j');
define('LOGGED_IN_KEY',    '.7%MqF|8rqE x.aV8).+`EJZ+w<~o2,i9yr=RBl/.fIbg[tx>A|B1[!|GdYwsY#:');
define('NONCE_KEY',        'F_HQ9!FJQk{IB+MuF`KUCMf=(emT!iRzN?NN`N9tqs+^!Qw4L%SXCwpf<VLku;&Y');
define('AUTH_SALT',        'ydq#$EoZ(LRSB>b<4|pp9mW?My#GH>BY3wcR<V+H@h5V#%YD5EYWZc7+rk4clP>2');
define('SECURE_AUTH_SALT', '6.k^Tv|T%#9r5ZLSO1WJz6X]77vtm<nMmOT1{HQnyO_PP2M(,Ew(5hFzCPAol NG');
define('LOGGED_IN_SALT',   'GJtsmU(;51[(SHW!F>.w$Q4L|e7tCQIHynyJ]N]~D3nUI4p|D JbbXAb$dXdNuj ');
define('NONCE_SALT',       ';Yz/A/nU|?h>p`s+J_gJyk#& #LIAqb&(F.jw?g]7J3)q$XIYT.fI5=~I3?PumvQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
